package TestBase;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import io.github.bonigarcia.wdm.ChromeDriverManager;
import io.github.bonigarcia.wdm.WebDriverManager;


//TestBase

public class TestBase {
	public static RemoteWebDriver driver;
	public static Properties prop;
	
	public TestBase() throws IOException
	{

	 prop = new Properties();
				FileInputStream ip = new FileInputStream("./properties/config.properties");
				prop.load(ip);
	}
	
	
	public void initialization() throws InterruptedException{
		String browserName = prop.getProperty("browser");
		
		
		if(browserName.equals("IE")){
			System.setProperty("webdriver.ie.driver", "C:\\Users\\Kumara\\Downloads\\SeleniumThings\\IEDriverServer_x64_3.14.0\\IEDriverServer.exe");	
			driver = new InternetExplorerDriver(); 
		}
		
		else if(browserName.equals("chrome")){
			//WebDriverManager.chromedriver().setup();
			
			System.out.println("expected method executed");
			
			System.setProperty("webdriver.chrome.driver", "C:\\\\Users\\\\Kumara\\\\Downloads\\\\SeleniumThings\\\\chromedriver_win32\\\\chromedriver.exe");	
			
			//System.setProperty("webdriver.chrome.driver","C:\\Users\\DURAI\\AppData\\Local\\Google\\Chrome\\Application\\chrome.exe");
			
			
			driver = new ChromeDriver(); 
		}
		
		
		
		else if(browserName.equals("FF")){
			System.setProperty("webdriver.gecko.driver", "/Users/naveenkhunteta/Documents/SeleniumServer/geckodriver");	
			driver = new FirefoxDriver(); 
		}
		
		
		
		
		
		driver.manage().window().maximize();
		driver.manage().deleteAllCookies();
		//driver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);
		//driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		
		driver.get(prop.getProperty("url"));
	
	}
	

}
